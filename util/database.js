const mysql = require('mysql2');

const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    database: 'dawbd_fj21',
    password: '',
});

module.exports = pool.promise();