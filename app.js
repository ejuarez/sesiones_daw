console.log('hola mundo');
console.log('nodemon es un loquillo');
console.log('cualquier cambio, se ejecuta luego luego');

const express = require('express');
const app = express();

const rutasMascotas = require('./routes/mascotas');
const rutasUsuarios = require('./routes/usuarios');

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');

const multer = require('multer');

const path = require('path');

const csrf = require('csurf');
const csrfProtection = csrf();

const csrfMiddleware = require('./util/csrf');

app.set('view engine', 'ejs');
app.set('views', 'views');

//fileStorage: Es nuestra constante de configuración para manejar el almacenamiento
const fileStorage = multer.diskStorage({
    destination: (request, file, callback) => {
        //'uploads': Es el directorio del servidor donde se subirán los archivos 
        callback(null, 'uploads');
    },
    filename: (request, file, callback) => {
        //aquí configuramos el nombre que queremos que tenga el archivo en el servidor, 
        //para que no haya problema si se suben 2 archivos con el mismo nombre concatenamos el timestamp
        callback(null, new Date().getMilliseconds() + '_' + file.originalname);
    },
});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//En el registro, pasamos la constante de configuración y
//usamos single porque es un sólo archivo el que vamos a subir, 
//pero hay diferentes opciones si se quieren subir varios archivos. 
//'archivo' es el nombre del input tipo file de la forma
app.use(multer({ storage: fileStorage }).single('imagen')); 


app.use(cookieParser());

app.use(session({
    secret: 'oc4ñejni2fkmci0j23fewrnmcoweer', 
    resave: false, //La sesión no se guardará en cada petición, sino sólo se guardará si algo cambió 
    saveUninitialized: false, //Asegura que no se guarde una sesión para una petición que no lo necesita
}));


app.use(express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

//Middleware
app.use((request, response, next) => {
    console.log('Middleware!');
    next(); //Le permite a la petición avanzar hacia el siguiente middleware
});

app.use(csrfProtection); 
app.use(csrfMiddleware);

app.use('/mascotas', rutasMascotas);
app.use('/usuarios', rutasUsuarios);

app.use('/ruta/tacoshinos', (request, response, next) => {
    response.send('Respuesta de la ruta "/ruta/tacoshinos"'); 
});

app.use('/ruta', (request, response, next) => {
    response.send('Respuesta de la ruta "/ruta"'); 
});

app.get('/', (request, response, next) => {
    response.send('Bienvenido al mejor lugar para mascotas'); 
});

app.use((request, response, next) => {
    console.log('Error 404');
    response.status(404);
    response.send('Lo sentimos, hay muchas mascotas perdidas como tú en este sitio...'); //Manda la respuesta
});

app.listen(3000);