console.log('Hola, mundo!');

const precio = 22;

console.info('El precio de los tacos es de $' + precio);

let cantidad_tacos = 6;

console.log('Te voy a mandar ' + cantidad_tacos + ' tacos');

let persona = "Lalo";

let descuento  = 0;

if (cantidad_tacos > 6 && persona === "Lalo") {
    descuento  = 0.1 * (cantidad_tacos * precio );
    console.info('Se te hizo un descuento de $' + descuento);
}

const por_pagar = cantidad_tacos * precio - descuento;

console.info("Por pagar: " + por_pagar);

const clientes = [];

clientes.push('Lalo');
clientes.push('Javier');
clientes.push('Julio');

for (let cliente of clientes) {
    console.log(cliente);
}

const menu = [];
menu.push({nombre: 'El Paisa', precio: 22});
menu.push({nombre: 'El Tacoshino', precio: 29});

console.log(menu);

window.alert('Bienvenido a Tacoshinos');

const ordenar = window.confirm('¿Desea ordenar?');

if(ordenar) {
    
    const tacos = window.prompt('¿Cuántos tacos vas a querer?');

    const calcular_cuenta = (num_tacos) => num_tacos * 22;
    console.log('La cuenta es de: $' + calcular_cuenta(tacos)); 

    console.assert(calcular_cuenta(1) === 22);
    console.assert(calcular_cuenta(2) === 44);
}


