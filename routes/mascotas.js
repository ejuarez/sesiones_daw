const express = require('express');
const router = express.Router();

const path = require('path');

const isAuth = require('../util/is-auth');

const mascotasController = require('../controllers/mascotas_controller');


router.get('/nueva-mascota', isAuth, mascotasController.getNuevaMascota);

router.post('/nueva-mascota', isAuth, mascotasController.postNuevaMascota);

router.get('/', isAuth, mascotasController.get);

router.get('/:mascota_id', isAuth, mascotasController.getMascota);

router.post('/eliminar', isAuth, mascotasController.postMascota);

router.get('/buscar/:criterio', isAuth, mascotasController.getBuscar);

module.exports = router;