const Mascota = require('../models/mascota');

exports.getNuevaMascota = (request, response, next) => {
    response.render('nueva_mascota', { 
        titulo: "Nueva mascota" 
    });
};

exports.postNuevaMascota = (request, response, next) => {

    console.log(request.file);
    const file_path = request.file.path;
    
    const mascota = new Mascota(request.body.nombre, request.body.descripcion, file_path);

    mascota.save()
        .then(() => {
            request.session.ultima_adopcion = request.body.nombre;
            response.redirect('/mascotas');
        }).catch( err => {
            console.log(err);
            response.redirect('/mascotas/nueva-mascota');    
        });
}

exports.get = (request, response, next) => {

    response.setHeader('Set-Cookie', 'mascotas=Ya vi que te gustan las mascotas; HttpOnly');

    console.log(request.cookies.mascotas);

    Mascota.fetchAll()
        .then(([rows, fieldData]) => {
            response.render('mascotas', { 
                mascotas: rows, 
                titulo: "Mascotas",
                ultima_adopcion: request.session.ultima_adopcion === undefined ? "No hay adopciones recientes" : request.session.ultima_adopcion
            });
        })
        .catch(err => console.log(err));
    
}

exports.getMascota = (request, response, next) => {

    const id = request.params.mascota_id;

    Mascota.fetchOne(id)
        .then(([rows, fieldData]) => {
            response.render('mascotas', { 
                mascotas: rows, 
                titulo: "Mascotas",
                ultima_adopcion: request.session.ultima_adopcion === undefined ? "No hay adopciones recientes" : request.session.ultima_adopcion
            });
        })
        .catch(err => console.log(err));
    
}

exports.postMascota = (request, response, next) => {

    console.log("Petición asíncrona reciba");
    console.log(request.body);
    console.log(request.body.mascota_id);

    Mascota.delete(request.body.mascota_id)
        .then(() => {
            Mascota.fetchAll()
                .then(([rows, fieldData]) => {
                    return response.status(200).json({mascotas: rows});
                })
                .catch(err => {
                    console.log(err)
                });
        }).catch((err) => {
            console.log(err);
            return response.status(500).json({message: "Internal Server Error"});
        });  
}

exports.getBuscar =  (request, response, next) => {

    console.log("Petición asíncrona reciba");
    console.log(request.params.criterio);

    Mascota.fetch(request.params.criterio)
        .then(([rows, fieldData]) => {
            return response.status(200).json({mascotas: rows});
        })
        .catch(err => {
            console.log(err)
        });
        
}